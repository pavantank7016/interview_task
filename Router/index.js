import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Button } from 'react-native';

// SCREENS
import Home from '../src/Home';
import PassengerDetails from '../src/PassengerDetails';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="PassengerDetails" component={PassengerDetails} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;