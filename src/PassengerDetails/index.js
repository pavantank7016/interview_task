import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  ActivityIndicator,
  Button,
  TextInput,
  Alert,
} from "react-native";
import { useNavigation } from "@react-navigation/native";

// API
import axios from "axios";

// STYLES
import styles from "./styles";

const index = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const [passengerDetails, setPassengerDetails] = useState({});
  const [airlineDetails, setAirlineDetails] = useState({});
  const [isEditable, setIsEditable] = useState(false);
  const [name, setName] = useState("");

  const navigation = useNavigation();

  const fetchPassengerDetails = (passengerID) => {
    setIsLoading(true);
    axios
      .get(`https://api.instantwebtools.net/v1/passenger/${passengerID}`)
      .then((response) => {
        console.log("RESPONSE ===>", response);
        if (response) {
          setIsLoading(false);
          setPassengerDetails(response?.data);
          setAirlineDetails(response?.data?.airline[0]);
          setName(response?.data?.name);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const updateDetails = () => {
    console.log(name);
    setIsEditable(false);

    const body = {
      name: name,
    };

    axios
      .patch(
        `https://api.instantwebtools.net/v1/passenger/${props.route.params.passengerID}`,
        body
      )
      .then((response) => {
        console.log(response.data);
        Alert.alert(response.data.message);
        setName(name);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button onPress={() => setIsEditable(true)} title="Edit" />
      ),
    });
  }, [navigation]);

  useEffect(async () => {
    console.log(
      "Passenger Details page - PROPS ===>",
      props.route.params.passengerID
    );
    await fetchPassengerDetails(props.route.params.passengerID);
  }, []);

  return (
    <View style={styles.mainContainer}>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <View>
          <Text style={styles.textLG}>Passenger Details</Text>
          {isEditable ? (
            <View>
              <TextInput
                style={styles.textMD}
                placeholder={passengerDetails.name}
                onChangeText={(value) => setName(value)}
                value={name}
              />
              <Button title="Update Name" onPress={updateDetails} />
            </View>
          ) : (
            <Text style={styles.textMD}>Passenger Name : {name}</Text>
          )}
          <Text style={styles.textMD}>Trips : {passengerDetails.trips}</Text>
          <Text style={[styles.textLG, styles.sectionHeader]}>
            Airline Details
          </Text>
          <View style={styles.airlineDetailsContainer}>
            <Image
              style={styles.airlineImage}
              source={{ uri: airlineDetails.logo }}
            />
            <View style={styles.airlineDetailsInner}>
              <Text style={styles.textMD}>
                Airline Name : {airlineDetails.name}
              </Text>
              <Text style={styles.textMD}>
                Airline Slogan : {airlineDetails.slogan}
              </Text>
              <Text style={styles.textMD}>
                Country : {airlineDetails.country}
              </Text>
              <Text style={styles.textMD}>
                Established In : {airlineDetails.established}
              </Text>
              <Text style={styles.textMD}>
                Headquarters: {airlineDetails.head_quaters}
              </Text>
              <Text style={styles.textMD}>
                Website: {airlineDetails.website}
              </Text>
            </View>
          </View>
        </View>
      )}
    </View>
  );
};

export default index;
