import { StyleSheet } from "react-native";

export default StyleSheet.create({
  // root styles
  mainContainer: {
    backgroundColor: '#fff',
    height: '100%',
    paddingVertical: 30,
    paddingHorizontal: 20
  },

  // details styling
  sectionHeader: {
    marginTop: 30
  },
  airlineDetailsContainer: {
    alignItems: 'center',
  },
  airlineDetailsInner: {
    // alignItems: 'center',
  },
  airlineImage: {
    height: 120,
    width: 200,
    resizeMode: 'contain'
  },
  textLG: {
    fontSize: 22,
    fontWeight: '600',
    marginBottom: 10
  },
  textMD: {
    fontSize: 20,
    marginBottom: 5,
    borderWidth: 1,
    borderColor: 'lightgray',
    padding: 10,
    marginVertical: 5
  }
})