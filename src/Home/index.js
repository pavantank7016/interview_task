import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";

// STYLES
import styles from "./styles";

// API
import axios from "axios";

const Home = ({ navigation }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [passengerData, setPassengerData] = useState([]);
  const [dataPerPage, setDataPerPage] = useState(10);

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      fetchPassengerData();
    });
    return unsubscribe;
  }, [navigation]);

  const fetchPassengerData = () => {
    setIsLoading(true);
    axios
      .get(
        `https://api.instantwebtools.net/v1/passenger?page=1&size=${dataPerPage}`
      )
      .then((response) => {
        console.log("RESPONSE ===>", response);
        if (response) {
          setPassengerData(response?.data?.data);
          setTimeout(() => {
            setIsLoading(false);
          }, 1000);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onItemPress = (passengerID) => {
    console.log(passengerID);
    navigation.navigate("PassengerDetails", { passengerID: passengerID });
  };

  const renderPassengerItem = ({ item, index }) => {
    const airlineData = item.airline[0];

    return (
      <TouchableOpacity
        style={styles.passengerItem}
        onPress={() => onItemPress(item._id)}
      >
        <View style={styles.passengerInnerContainer}>
          <View style={styles.imageContainer}>
            <Image
              source={{ uri: airlineData.logo }}
              style={styles.airlineImage}
            />
          </View>
          <View style={styles.innerContainer}>
            <Text style={styles.passengerName}>{item.name}</Text>
            <Text style={styles.trip}>{item.trips}</Text>
          </View>
        </View>
        <Text
          style={styles.note}
        >{`${airlineData.name} - ${airlineData.slogan}`}</Text>
      </TouchableOpacity>
    );
  };

  const renderFlatList = () => {
    return (
      <FlatList
        data={passengerData}
        keyExtractor={(item, index) => `${item} - ${index}`}
        renderItem={renderPassengerItem}
        onEndReached={() => {
          setDataPerPage(dataPerPage + 10);
          fetchPassengerData();
        }}
        onEndThreshold={0}
        ListFooterComponent={isLoading && <ActivityIndicator />}
      />
    );
  };

  return <View style={styles.mainContainer}>{renderFlatList()}</View>;
};

export default Home;
