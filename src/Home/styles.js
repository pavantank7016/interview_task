import { StyleSheet, Platform } from "react-native";

const isIOSDevice = Platform.OS === "ios";

export default StyleSheet.create({
  // root styles
  mainContainer: {
    // paddingTop: isIOSDevice ? 50 : 0,
    paddingTop: 10
  },

  // styles for passenger item
  passengerItem: {
    padding: 8,
    margin: 10,
    marginHorizontal: 15,
    borderRadius: 10,
    borderBottomColor: "lightgray",
    borderBottomWidth: 1,
    backgroundColor: '#fff',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  passengerInnerContainer: {
    flexDirection: "row",
  },
  imageContainer: {
    padding: 8,
    borderColor: "lightgray",
    borderWidth: 1,
    borderRadius: 10,
  },
  airlineImage: {
    height: 75,
    width: 75,
    resizeMode: "contain",
  },
  innerContainer: {
    flex: 1,
    paddingHorizontal: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  passengerName: {
    fontSize: 20,
    fontWeight: "700",
  },
  trip: {
    fontSize: 25,
    fontWeight: "500",
    color: "green",
  },
  note: {
    fontSize: 18,
    textAlign: "center",
    marginTop: 10,
    color: "gray",
  },
});
